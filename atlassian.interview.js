//object for all react components
let atlassian = {
    init: class init extends React.Component{
        constructor(){
            super()
            //set initial values
            this.state = {
                showCards:false,
                loading:false,//not using, but would normally
                origCards: [],
                cards: []
            }
        }

        componentDidMount(){
            //get cards data
            this.getCards(()=>{
                //set showCards to render cards
                this.setState({showCards:true})
            })
        }

        getCards = (callback) =>{
            // let _url = "http://messagebot.ngrok.io/javascripts/data-set.json";
            console.log( `dataUrl ${dataUrl}` )
            //get data for cards
            atlassian.fetchPost(dataUrl, (err, response) =>{
                //if error load local var data
                let dataCards =  tmpDataHolder
                //check if error to load response
                if(!err){
                    dataCards = response
                }else{
                    console.log("error")
                    // console.log(tmpDataHolder)
                }
                //set cards data
                this.setState({
                    cards:dataCards,
                    origCards:dataCards
                })
                callback()
            })
        }

        filterCards = ( value ) =>{
            //filter cards based on selection
            let origCards = this.state.origCards
            //if filter return cards with filtered
            let filtered = (value.length > 0) ? origCards.filter( card =>  card.category == value ) : origCards
            //update cards
            this.setState({cards:filtered})
        }

        render(){
            return(
                <div>
                    <atlassian.renderCategoryFilter filterCards={this.filterCards} cards={this.state.cards}/>
                    {this.state.showCards ? <atlassian.renderCards cards={this.state.cards}/> : <div className="test-loading">Loading...</div>}
                </div>
            )
        }
    },
    renderCategoryFilter: class renderCategoryFilter extends React.Component{
        setFilter = (event) =>{
            //handle category event for filtering
            this.props.filterCards(event.target.value)
        }

        render(){
            //render category select form
            return(
                <div className="category-form">
                    <div className="filter-txt">Filter Play By</div>
                    <div className="category-txt">Category</div>
                    <select className="form-control" id="sel-category" onChange={this.setFilter} >
                        {/*could be built from cards*/}
                        <option value="">Any</option>
                        <option value="leadership">Leadership</option>
                        <option value="project">Project</option>
                        <option value="customer">Customer</option>
                    </select>
                </div>
            )
        }
    },
    renderCards: class renderCards extends React.Component{

        getCardRows(array, size =3) {
            //make list into rows of 3
            const chunked_arr = [];
            let copied = [...array];
            const numOfChild = Math.ceil(copied.length / size); // Round up to the nearest integer
            for (let i = 0; i < numOfChild; i++) {
                chunked_arr.push(copied.splice(0, size));
            }
            return chunked_arr;
        }


        buildList(){
            let cards = this.props.cards
            //create array with rows
            let cardRows = this.getCardRows(cards)
            // console.log(cardsRow)
            return(
                //loop over rows
                cardRows.map( cardRow => {
                    return(<atlassian.renderCardRows cards={cardRow} />)
                })
            )
        }
        render(){
            return(
                <div className="container">
                    {this.buildList()}
                </div>
            )
        }
    },
    renderCardRows:class renderCardRows extends React.Component{

        renderCard(card){
            //renders each card
            const {title, description, link, image} = card
            // console.log(`tiltle ${title}`)
            return(
                <div className="card-outer">
                    <img src={image} />
                    <div className="card-title">{title}</div>
                    <div className="card-desc">{description}</div>
                    <a href={link} target="_blank">Learn More</a>
                </div>
            )
        }

        render(){
            let cards = this.props.cards
            return(
                <div className="row" >
                    {cards.map( card => {
                        // console.log()
                        return(<div className="col-xs-4">{this.renderCard(card)}</div>)
                    } )}
                </div>
            )
        }
    },
    fetchPost:(url, callback) =>{
        //generic fetch data
        fetch(url, {
            // method: 'post',
            // body: JSON.stringify(body),
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
        }).then(res=>res.json())
            .then(res => {
                // console.log(res)
                callback(null, res) ;
            })
            .catch(function() {
                console.log("error");
                callback(true,[])
            });;
    }
}

//init
ReactDOM.render(
    <atlassian.init />,
    document.getElementById("root")
);

//incase data can't load
let tmpDataHolder = [
    {
        "title": "Capacity Planning",
        "description": "Create a capacity plan for your team, with your team.",
        "link": "https://example.com/capacity-planning",
        "category": "leadership",
        "image": "https://www.atlassian.com/dam/jcr:fa55c05f-c087-4d8f-9f9b-4615216ca4db/newplay.png"
    },
    {
        "title": "5 Whys Analysis",
        "description": "Uncover the root of a problem and explore possible solutions.",
        "link": "https://example.com/5-whys",
        "category": "project",
        "image": "https://www.atlassian.com/dam/jcr:8b081c1f-6a30-46a8-8683-b149a5fa0e2b/5whys.png"
    },
    {
        "title": "Change Management Kick-off",
        "description": "Align your team and create a plan of action.",
        "link": "https://example.com/change-management",
        "category": "project",
        "image": "https://www.atlassian.com/dam/jcr:c9760c7e-831a-4438-ad5c-8618cc9051df/customer-journey-mapping.png"
    },
    {
        "title": "Demo Trust",
        "description": "Get feedback from your leadership team when it matters most.",
        "link": "https://example.com/demo-trust",
        "category": "leadership",
        "image": "https://www.atlassian.com/dam/jcr:8062a8eb-1e5e-41e0-8f22-72a6b79f3006/demotrust.png"
    },
    {
        "title": "Empathy Mapping",
        "description": "Align your success criteria with the needs of customer personas.",
        "link": "https://example.com/empathy-mapping",
        "category": "customer",
        "image": "https://www.atlassian.com/dam/jcr:e3272b7f-f0b3-4c63-8324-d35ed7736918/empathymapping.png"
    },
    {
        "title": "End-to-end Demo",
        "description": "Visualize your project and get feedback at every stage.",
        "link": "https://example.com/e2e-demo",
        "category": "project",
        "image": "https://www.atlassian.com/dam/jcr:6de0a6ac-b7d7-427d-8c00-00038c176276/endtoend.png"
    },
    {
        "title": "Incident Response Communications",
        "description": "Don't just fix the problem � keep customers updated, too.",
        "link": "https://example.com/incident-response",
        "category": "customer",
        "image": "https://www.atlassian.com/dam/jcr:8e913621-97b2-420d-8470-53491045d1bd/Incident%20communication%20reviews.svg"
    }
]